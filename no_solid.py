# Issues
# Main focus: prevent consequences of the "ripple-effect" from software changes
# 1. Mixed responsibilities: Card is a DECK, actual card is a tuple
# 2. Missing responsibilities: how hand's total points are computed?
# 3. Limited reuse potential: extension for other games?
# 4. No substitution: impossible to use Shoe instead of Card
# 5. Poor constructor design: classes are incompatible,
# no easy way to write a simulator testing strategy on 1 deck and 1 shoe
# 6. Haphazard interface: classes both have shuffling feature

# Solution
# 1. Highlight original class features (should focus on fewer actions)
#    - Create Deck/Shoe of Card instances (poorly fits in Card class interface)
#    - Shuffle & deal cards (poorly fits in Card class interface)
#    - Count points of each card (specific only to blackjack)

# 2. Decompose the interface (no need to change a class to add a new feature)
#    - Card: rank & suit, used by player (interface shared between all card-related processes)
#    - Deck: shuffles & deals collection of cards (interface shared between all Deck/Shoe processes)
#    - Strategy classes: game-specific features (interfaces for blackjack points)

# 3. Create new implementation

import random


class Card:
    def __init__(self):
        self.cards = [(rank, suit) for rank in range(1, 14) for suit in '♠♡♢♣']
        random.shuffle(self.cards)

    def deal(self):
        return self.cards.pop()

    def points(self, card):
        rank, suit = card
        if rank == 1:
            return 1, 11
        elif 2 <= rank < 11:
            return rank, rank
        else:
            return 10, 10


class Shoe(Card):
    def __init__(self, n):
        super().__init__()
        self.shoe = []
        for _ in range(n):
            self.shoe.extend(self.cards)
        random.shuffle(self.shoe)

    def shuffle_burn(self, n=100):
        random.shuffle(self.shoe)
        self.shoe = self.shoe[n:]

    def deal(self):
        return self.shoe.pop()


__test__ = {
    'Card': '''
>>> random.seed(1) # Deterministic Sequence
>>> deck = Card()
>>> c1 = deck.deal()
>>> c1
(3, '♠')
>>> hand = [deck.deal() for _ in range(5)]
>>> hand
[(10, '♠'), (13, '♠'), (2, '♠'), (5, '♠'), (2, '♣')]

# Awkward totalling
>>> hand2 = [(11, '♠'), (1, '♠')]
>>> sum(deck.points(c)[0] for c in hand2)
11
>>> sum(deck.points(c)[1] for c in hand2)
21
''',

    'Shoe': '''
>>> deck = Shoe(6)
>>> random.seed(1) # Deterministic Sequence
>>> deck.shuffle_burn(100)
>>> hand = [deck.deal() for _ in range(5)]
>>> hand
[(12, '♣'), (10, '♡'), (7, '♢'), (9, '♢'), (4, '♣')]
>>> dealing= True
>>> while dealing:
...     try:
...        hand = [deck.deal() for _ in range(5)]
...        eq = any( hand[c1] == hand[c2]
...           for c1 in range(len(hand)) for c2 in range(len(hand)) if c1 != c2)
...        if eq: print(hand)
...     except IndexError:
...        dealing= False
...
[(6, '♢'), (5, '♡'), (6, '♢'), (13, '♡'), (9, '♡')]
[(3, '♢'), (9, '♠'), (6, '♡'), (2, '♢'), (9, '♠')]
''',
}

if __name__ == "__main__":
    import doctest

    doctest.testmod(verbose=1)
